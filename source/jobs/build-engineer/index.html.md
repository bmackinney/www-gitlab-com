---
layout: job_page
title: "Build Engineer"
---


## Responsibilities

We are currently hiring self-directed, communicative, experienced build engineers to join GitLab. Our build team closely partners with the rest of the engineering organization to build, configure, and automate GitLab installation. GitLab's build team is tasked with creating a seamless installation experience for customers and community users. [GitLab Omnibus](https://gitlab.com/gitlab-org/omnibus-gitlab) project enables our users to install, upgrade, and use GitLab easily, and the build team's primary goal is maintaining and improving Omnibus and the infrastructure around it.

Build engineering is interlaced with the broader development team in supporting newly created features and resolving bugs on the omnibus-gitlab project side. Notably, the infrastructure team is the build team's biggest internal customer, so there is significant team interdependency. This team also provides significant variety in tasks and access to a diversity of projects, including helping out on various community packaging projects, etc.  

For a better understanding of how we work, check out the our handbook under [GitLab Workflow](https://about.gitlab.com/handbook/communication/#prioritize).

## Requirements

* Chef experience (writing complex cookbooks from scratch, custom providers, custom resources, etc.)
* Production level Ruby/RoR
* Extensive Linux experience, comfortable between Debian and RHEL based systems
* Experience with Docker in production use cases
* Basic knowledge of packaging archives such as .deb and .rpm package archives
* An inclination towards communication, inclusion, and visibility
* Experience owning a project from concept to production, including proposal, discussion, and execution.
* English written and verbal communication skills
* Demonstrated ability to work closely with other parts of the organization
* Share our values, and work in accordance with those values

## Hiring Process

Applicants for this position can expect the hiring process to follow the order below. Please keep in mind that applicants can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find her/his job title on our [team page](/team).

* Qualified applicants receive a short technical prompt from our Global Recruiters
* The review process for this role can take a few days, check in with the Global recruiter at any point to learn the status of your application
* Selected candidates will be invited to schedule a 45min [screening call](/handbook/hiring/#screening-call) with our Global Recruiters
* Next, candidates will be invited to schedule a first technical interview with our Build Lead
* Successful candidates will be asked to provide 2-3 references
* Candidates will be invited to schedule a third interview with our VP of Engineering
* Finally, candidates will interview with our CEO
* Successful candidates will subsequently be made an offer via email

Additional details about our process can be found on our [hiring page](/handbook/hiring).
